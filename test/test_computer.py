import pytest
from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton, QMessageBox)
from gui.mainwin import Ui_MainWindow

from PyQt5.QtCore import QEventLoop, QTime, QTimer
from unittest.mock import MagicMock


def test_computer_move(qtbot):
    main_window = Ui_MainWindow()
    ui = QMainWindow()
    main_window.setupUi(ui)
    qtbot.addWidget(main_window)

    # Set up the game mode and player buttons
    main_window.generalGame_radioButton.setChecked(True)
    main_window.blueComputerPlayer.setChecked(True)
    main_window.redComputerPlayer.setChecked(True)

    rows = int(main_window.boardSize_lineEdit.text())
    columns = int(main_window.boardSize_lineEdit.text())

    # Call makeMove to simulate the computer's move
    main_window.makeMove("blue")

    # Use an event loop to wait for the QTimer.singleShot() to finish
    loop = QEventLoop()
    QTimer.singleShot(750, loop.quit)
    loop.exec_()

    # Check that the computer made a valid move
    move_made = False
    for row in range(rows):
        for col in range(columns):
            if main_window.buttons[(row, col)].text() in ["S", "O"]:
                move_made = True
                break
        if move_made:
            break
    assert move_made, "The computer didn't make a valid move."


