from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton, QMessageBox)
from PyQt5 import QtCore
from gui.mainwin import Ui_MainWindow
from unittest.mock import MagicMock

def test_simple_game_blue_win(qtbot):
    sos_game = Ui_MainWindow()
    ui = QMainWindow()
    sos_game.setupUi(ui)
    qtbot.addWidget(sos_game)

    # Set up the game
    sos_game.boardSize_lineEdit.setText("3")
    sos_game.simpleGame_radioButton.setChecked(True)
    sos_game.buildTheBoard()

    # Mock the QMesageBox
    sos_game.showMessageBox = MagicMock()

    # Player blue places an 'S' at (0, 0)
    sos_game.bluePlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 0)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (1, 0)
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 0)], QtCore.Qt.LeftButton)

    # Player blue places an 'O' at (0, 1)
    sos_game.bluePlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 1)], QtCore.Qt.LeftButton)

    # Player red places an 'O' at (1, 1)
    sos_game.redPlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 1)], QtCore.Qt.LeftButton)

    # Player blue places an 'S' at (0, 2) to create a match and win the game
    sos_game.bluePlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 2)], QtCore.Qt.LeftButton)
    
    sos_game.showMessageBox.assert_called_once_with("Blue has won the game!")

def test_simple_game_red_win(qtbot):
    sos_game = Ui_MainWindow()
    ui = QMainWindow()
    sos_game.setupUi(ui)
    qtbot.addWidget(sos_game)

    # Set up the game
    sos_game.boardSize_lineEdit.setText("3")
    #qtbot.mouseClick(sos_game.generalGame_radioButton, QtCore.Qt.LeftButton)
    qtbot.mouseClick(sos_game.simpleGame_radioButton, QtCore.Qt.LeftButton)
    #sos_game.simpleGame_radioButton.setChecked(True)
    sos_game.buildTheBoard()

    # Mock the QMesageBox
    #sos_game.showMessageBox.reset_mock()
    sos_game.showMessageBox = MagicMock()

    # Player blue places an 'S' at (0, 0)
    sos_game.bluePlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 0)], QtCore.Qt.LeftButton)

    # Player red places an 'O' at (1, 0)
    sos_game.redPlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 0)], QtCore.Qt.LeftButton)

    # Player blue places an 'O' at (0, 1)
    sos_game.bluePlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 1)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (2, 0) to create a match and win the game
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(2, 0)], QtCore.Qt.LeftButton)

    sos_game.showMessageBox.assert_called_once_with("Red has won the game!")


def test_general_game_blue_win(qtbot):
    sos_game = Ui_MainWindow()
    ui = QMainWindow()
    sos_game.setupUi(ui)
    qtbot.addWidget(sos_game)

    # Set up the game
    sos_game.boardSize_lineEdit.setText("3")
    sos_game.generalGame_radioButton.setChecked(True)
    sos_game.buildTheBoard()

    # Mock the QMessageBox
    sos_game.showMessageBox = MagicMock()

    # Player blue places an 'S' at (0, 0)
    sos_game.bluePlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 0)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (1, 0)
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 0)], QtCore.Qt.LeftButton)

    # Player blue places an 'O' at (0, 1)
    sos_game.bluePlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 1)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (1, 1)
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 1)], QtCore.Qt.LeftButton)

    # Player blue places an 'S' at (0, 2) to create a match
    sos_game.bluePlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 2)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (2, 0)
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(2, 0)], QtCore.Qt.LeftButton)

    # Player blue places an 'S' at (2, 1)
    sos_game.bluePlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(2, 1)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (2, 2)
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(2, 2)], QtCore.Qt.LeftButton)

    # Player blue places an 'S' at (1, 2)
    sos_game.bluePlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 2)], QtCore.Qt.LeftButton)

    # Check if the QMessageBox displays the correct message    
    sos_game.showMessageBox.assert_called_once_with("Blue Wins!! Blue scored 1 matches.")

def test_general_game_red_win(qtbot):
    sos_game = Ui_MainWindow()
    ui = QMainWindow()
    sos_game.setupUi(ui)
    qtbot.addWidget(sos_game)

    # Set up the game
    sos_game.boardSize_lineEdit.setText("3")
    sos_game.generalGame_radioButton.setChecked(True)
    sos_game.buildTheBoard()

    # Mock the QMessageBox
    sos_game.showMessageBox = MagicMock()

    # Player blue places an 'O' at (0, 0)
    sos_game.bluePlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 0)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (1, 0)
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 0)], QtCore.Qt.LeftButton)

    # Player blue places an 'O' at (0, 1)
    sos_game.bluePlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 1)], QtCore.Qt.LeftButton)

    # Player red places an 'O' at (1, 1)
    sos_game.redPlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 1)], QtCore.Qt.LeftButton)

    # Player blue places an 'O' at (0, 2) 
    sos_game.bluePlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(0, 2)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (2, 0)
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(2, 0)], QtCore.Qt.LeftButton)

    # Player blue places an 'O' at (2, 1)
    sos_game.bluePlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(2, 1)], QtCore.Qt.LeftButton)

    # Player red places an 'S' at (1, 2) to create a match
    sos_game.redPlayerS_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(1, 2)], QtCore.Qt.LeftButton)

    # Player blue places an 'O' at (2, 2)
    sos_game.bluePlayerO_radioButton.setChecked(True)
    qtbot.mouseClick(sos_game.buttons[(2, 2)], QtCore.Qt.LeftButton)

    # Check if the QMessageBox displays the correct message    
    sos_game.showMessageBox.assert_called_once_with("Red Wins!! Red scored 1 matches.")


