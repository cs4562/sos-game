from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton, QMessageBox)
from PyQt5 import QtCore
from gui.mainwin import Ui_MainWindow
from unittest.mock import MagicMock

def test_simple_move(qtbot):
    # Create an instance of the UI and add it to the test fixture
    main_window = Ui_MainWindow()
    ui = QMainWindow()
    main_window.setupUi(ui)
    qtbot.addWidget(main_window)

    # Set up the game mode and player Buttons
    qtbot.mouseClick(main_window.simpleGame_radioButton, QtCore.Qt.LeftButton)
    rows = int(main_window.boardSize_lineEdit.text())
    columns = int(main_window.boardSize_lineEdit.text())
    main_window.redPlayerO_radioButton.setChecked(True)
    main_window.bluePlayerO_radioButton.setChecked(True)
    
    main_window.showMessageBox = MagicMock()
    # Make a move for each button on the board and check to make sure it's populated with the correct value
    for row in range(rows):
        for col in range(columns):
            qtbot.mouseClick(main_window.buttons[(row, col)], QtCore.Qt.LeftButton)
            assert main_window.buttons[(row,col)].text() == 'O'
    main_window.showMessageBox.assert_called_once_with("Tie Game")
    

    # Creating a blank board and switches the player buttons
    main_window.buildTheBoard()
    main_window.redPlayerS_radioButton.setChecked(True)
    main_window.bluePlayerS_radioButton.setChecked(True)

    main_window.showMessageBox = MagicMock()
    main_window.showMessageBox.mock_reset()
    # Make a move for each button on the board and check to make sure it's populated with the correct value
    for row in range(rows):
        for col in range(columns):
            qtbot.mouseClick(main_window.buttons[(row,col)], QtCore.Qt.LeftButton)
            assert main_window.buttons[(row, col)].text() == 'S'
    main_window.showMessageBox.assert_called_once_with("Tie Game")

def test_general_move(qtbot):
    # Create an instance of the UI and add it to the test fixture
    main_window = Ui_MainWindow()
    ui = QMainWindow()
    main_window.setupUi(ui)
    qtbot.addWidget(main_window)

    # Set up the game mode and player Buttons 
    qtbot.mouseClick(main_window.generalGame_radioButton, QtCore.Qt.LeftButton)
    rows = int(main_window.boardSize_lineEdit.text())
    columns = int(main_window.boardSize_lineEdit.text())
    main_window.redPlayerO_radioButton.setChecked(True)
    main_window.bluePlayerO_radioButton.setChecked(True)
    
    main_window.showMessageBox = MagicMock()
    # Make a move for each button on the board and check to make sure it's populated with the correct value
    for row in range(rows):
        for col in range(columns):
            qtbot.mouseClick(main_window.buttons[(row, col)], QtCore.Qt.LeftButton)
            assert main_window.buttons[(row,col)].text() == 'O'
    main_window.showMessageBox.assert_called_once_with("Tie Game")
    main_window.showMessageBox.reset_mock()
    # Creating a blank board and switches the player buttons
    main_window.buildTheBoard()

    main_window.redPlayerS_radioButton.setChecked(True)
    main_window.bluePlayerS_radioButton.setChecked(True)

    # Make a move for each button on the board and check to make sure it's populated with the correct value
    for row in range(rows):
        for col in range(columns):
            qtbot.mouseClick(main_window.buttons[(row,col)], QtCore.Qt.LeftButton)
            assert main_window.buttons[(row, col)].text() == 'S'
    main_window.showMessageBox.assert_called_once_with("Tie Game")


