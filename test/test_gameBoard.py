from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton, QMessageBox)
from PyQt5 import QtCore
from gui.mainwin import Ui_MainWindow
from unittest.mock import MagicMock



# Define test functions using the pytest-qt plugin
def test_build_the_board(qtbot):
    # Create an instance of the UI and add it to the test fixture
    main_window = Ui_MainWindow()
    ui = QMainWindow()
    main_window.setupUi(ui)
    qtbot.addWidget(main_window)

    # Check that the buttons were created with the correct number of rows and columns
    rows = int(main_window.boardSize_lineEdit.text())
    columns = int(main_window.boardSize_lineEdit.text())
    assert main_window.gridLayout.rowCount() == rows
    assert main_window.gridLayout.columnCount() == columns

    # Check that each button was created
    for row in range(rows):
        for col in range(columns):
            button = main_window.buttons.get((row, col))
            assert button is not None
            assert isinstance(button, QPushButton)

def test_game_mode(qtbot):
    # Create an instance of the UI and add it to the test fixture
    main_window = Ui_MainWindow()
    ui = QMainWindow()
    main_window.setupUi(ui)
    qtbot.addWidget(main_window)
    

    # Test the "Simple Game" radio button
    qtbot.mouseClick(main_window.simpleGame_radioButton, QtCore.Qt.LeftButton)
    assert main_window.simpleGame_radioButton.isChecked() == True
    assert main_window.simple_game == True

    # Test the "General Game" radio button
    qtbot.mouseClick(main_window.generalGame_radioButton, QtCore.Qt.LeftButton)
    assert main_window.generalGame_radioButton.isChecked() == True
    assert main_window.general_game == True


def test_new_game_board_size(qtbot):
    # Create an instance of the UI and add it to the test fixture
    main_window = Ui_MainWindow()
    ui = QMainWindow()
    main_window.setupUi(ui)
    qtbot.addWidget(main_window)

    # Set the board size to 4 and submit
    main_window.boardSize_lineEdit.setText("4")
    main_window.areYouSure = MagicMock()
    qtbot.mouseClick(main_window.submit_pushButton, QtCore.Qt.LeftButton)

    #qtbot.mouseClick(msg.Ok, QtCore.Qt.LeftButton)
    main_window.areYouSure.assert_called_once_with()

    # Call the "test_build_the_board" function to check that the board was created correctly 
    test_build_the_board(qtbot)


