from PyQt5 import QtCore

from PyQt5.QtGui import (
    QFont,
    QPalette,
    QColor,
    QKeySequence,
    )

from PyQt5.QtWidgets import (
    QWidget,
    QPushButton,
    QLabel,
    QVBoxLayout,
    QHBoxLayout,
    QGridLayout,
    QLineEdit,
    QRadioButton,
    QSizePolicy,
    QMenuBar,
    QStatusBar,
    QMessageBox,
    QComboBox,
    QCheckBox,
    QShortcut,

)
import random
import json
from FocusButton import FocusButton
from GameLogic import GameLogic

import time
from PyQt5.QtCore import QTimer



class Ui_MainWindow(QWidget):
    def setupUi(self, MainWindow):
        self.MainWindow = MainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.setMinimumSize(QtCore.QSize(940, 650))
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        
        self.appBoardLayout = QGridLayout(self.centralwidget)
        self.appBoardLayout.setContentsMargins(0, 0, 0, 0)
        self.appBoardLayout.setObjectName("appBoardLayout")
              
        
        #Size Policy
        playerLabelSizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        computerPlayerSizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        matchesSizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        playerRadioButtonSizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        
        # Playable Area
        self.gamePlayLayoutWidget = QWidget(self.centralwidget)
        self.gamePlayLayoutWidget.setObjectName("gamePlayLayoutWidget")
        self.gamePlayLayout = QGridLayout(self.gamePlayLayoutWidget)
        self.gamePlayLayout.setContentsMargins(0, 0, 0, 0)
        self.gamePlayLayout.setObjectName("gamePlayLayout")
        
        self.appBoardLayout.addWidget(self.gamePlayLayoutWidget, 1, 1, 3, 3)
        # Position: Row 0, Column 0. Span: 3 rows, 3 column

        #Blue Player Box
        self.bluePlayerBoxWidget = QWidget(self.centralwidget)
        self.bluePlayerBoxWidget.setGeometry(QtCore.QRect(20, 190, 160, 221))
        self.bluePlayerBoxWidget.setObjectName("bluePlayerBoxWidget")
        self.bluePlayerBox = QVBoxLayout(self.bluePlayerBoxWidget)
        self.bluePlayerBox.setObjectName("bluePlayerBox")    
        self.appBoardLayout.addWidget(self.bluePlayerBoxWidget, 1, 0, 3, 1)
        # Position: Row 1, Column 0. Span: 3 rows, 1 column

        
        # Blue Player
        self.bluePlayer_abel = QLabel(self.bluePlayerBoxWidget)
        self.bluePlayer_abel.setSizePolicy(playerLabelSizePolicy)
        font = QFont()
        font.setPointSize(15)
        self.bluePlayer_abel.setFont(font)
        self.bluePlayer_abel.setObjectName("bluePlayer_abel")
        self.bluePlayerBox.addWidget(self.bluePlayer_abel)
        
        # Blue Computer
        self.blueComputerPlayer = QRadioButton(self.bluePlayerBoxWidget)
        self.blueComputerPlayer.setObjectName("blueComputerPlayer")
        self.bluePlayerBox.addWidget(self.blueComputerPlayer)
        self.blueComputerPlayer.clicked.connect(lambda: self.gameLogic.makeMove("blue"))
        self.blueComputerPlayer.setSizePolicy(computerPlayerSizePolicy)
        
        # Blue Player Matches
        self.bluePlayerMatches = 0
        self.bluePlayerMatches_label = QLabel(self.bluePlayerBoxWidget)
        self.bluePlayerMatches_label.setText(f'Blue Matches: {self.bluePlayerMatches}')
        self.bluePlayerMatches_label.setSizePolicy(matchesSizePolicy)
        self.bluePlayerMatches_label.setObjectName("bluePlayerMatches")
        self.bluePlayerBox.addWidget(self.bluePlayerMatches_label)
        
        # Blue S Button
        self.bluePlayerS_radioButton = QRadioButton(self.bluePlayerBoxWidget)
        self.bluePlayerS_radioButton.setObjectName("bluePlayerS_radioButton")
        self.bluePlayerS_radioButton.setChecked(True)
        self.bluePlayerS_radioButton.setSizePolicy(playerRadioButtonSizePolicy)
        self.bluePlayerBox.addWidget(self.bluePlayerS_radioButton)
        
        # Blue O Button
        self.bluePlayerO_radioButton = QRadioButton(self.bluePlayerBoxWidget)
        self.bluePlayerO_radioButton.setSizePolicy(playerRadioButtonSizePolicy)
        self.bluePlayerO_radioButton.setObjectName("bluePlayerO_radioButton")
        self.bluePlayerBox.addWidget(self.bluePlayerO_radioButton)

        # Red Player Box
        self.redPlayerBoxWidget = QWidget(self.centralwidget)
        self.redPlayerBoxWidget.setObjectName("redPlayerBoxWidget")
        self.redPlayerBox = QVBoxLayout(self.redPlayerBoxWidget)
        
        self.redPlayerBox.setContentsMargins(0, 0, 0, 0)
        self.appBoardLayout.addWidget(self.redPlayerBoxWidget, 1, 4, 3, 1)
        # Position: Row 1, Column 5. Span: 3 rows, 1 column

        self.redPlayerBox.setObjectName("redPlayerBox")
        
        # Red Player
        self.redPlayer_label = QLabel(self.redPlayerBoxWidget)
        self.redPlayer_label.setSizePolicy(playerLabelSizePolicy)
        self.redPlayer_label.setObjectName("redPlayer_label")
        self.redPlayerBox.addWidget(self.redPlayer_label)
        
        # Red Computer
        self.redComputerPlayer = QRadioButton(self.redPlayerBoxWidget)
        self.redComputerPlayer.setSizePolicy(computerPlayerSizePolicy)
        self.redComputerPlayer.setObjectName("redComputerPlayer")
        self.redPlayerBox.addWidget(self.redComputerPlayer)
        self.redComputerPlayer.clicked.connect(lambda: self.gameLogic.makeMove("red"))
        
        # Red Matches
        self.redPlayerMatches = 0
        self.redPlayerMatches_label = QLabel(self.redPlayerBoxWidget)
        self.redPlayerMatches_label.setText(f'Red Matches: {self.redPlayerMatches}')
        self.redPlayerMatches_label.setSizePolicy(matchesSizePolicy)
        self.redPlayerMatches_label.setObjectName("redPlayerMatches")
        self.redPlayerBox.addWidget(self.redPlayerMatches_label)
        
        
        # Red S Button
        self.redPlayerS_radioButton = QRadioButton(self.redPlayerBoxWidget)
        self.redPlayerS_radioButton.setSizePolicy(playerRadioButtonSizePolicy)
        self.redPlayerS_radioButton.setObjectName("redPlayerS_radioButton")
        self.redPlayerS_radioButton.setChecked(True)
        self.redPlayerBox.addWidget(self.redPlayerS_radioButton)
        
        # Red O Button
        self.redPlayerO_radioButton = QRadioButton(self.redPlayerBoxWidget)
        self.redPlayerO_radioButton.setSizePolicy(playerRadioButtonSizePolicy)
        self.redPlayerO_radioButton.setObjectName("redPlayerO_radioButton")
        self.redPlayerBox.addWidget(self.redPlayerO_radioButton)

        # Game Label/Options
        self.titleBarGameWidget = QWidget(self.centralwidget)
        self.titleBarGameWidget.setObjectName("titleBarGameWidget")
        self.titleBarGame = QHBoxLayout(self.titleBarGameWidget)
        self.titleBarGame.setContentsMargins(0, 0, 0, 0)

        self.appBoardLayout.addWidget(self.titleBarGameWidget, 0, 0, 1, 5)
        # Position: Row 0, Column 0. Span: 5 rows, 1 column


        self.titleBarGame.setObjectName("titleBarGame")
        # SOS
        self.gameName_label = QLabel(self.titleBarGameWidget)
        self.gameName_label.setAlignment(QtCore.Qt.AlignCenter)
        self.gameName_label.setObjectName("gameName_label")
        self.titleBarGame.addWidget(self.gameName_label)
        # Simple Game Option
        self.simpleGame_radioButton = QRadioButton(self.titleBarGameWidget)
        self.simpleGame_radioButton.setObjectName("simpleGame_radioButton")
        self.titleBarGame.addWidget(self.simpleGame_radioButton)
        self.simpleGame_radioButton.setChecked(True) 
        # General Game Option
        self.generalGame_radioButton = QRadioButton(self.titleBarGameWidget)
        self.generalGame_radioButton.setObjectName("generalGame_radioButton")
        self.titleBarGame.addWidget(self.generalGame_radioButton)
        # Board Size
        self.boardSize_label = QLabel(self.titleBarGameWidget)
        self.boardSize_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.boardSize_label.setObjectName("boardSize_label")
        self.titleBarGame.addWidget(self.boardSize_label)
        # Adjustable Board Size
        self.boardSize_lineEdit = QLineEdit(self.titleBarGameWidget)
        self.boardSize_lineEdit.setInputMask("")
        self.boardSize_lineEdit.setText("3")
        self.boardSize_lineEdit.setEchoMode(QLineEdit.Normal)
        self.boardSize_lineEdit.setCursorMoveStyle(QtCore.Qt.LogicalMoveStyle)
        self.boardSize_lineEdit.setObjectName("boardSize_lineEdit")
        self.titleBarGame.addWidget(self.boardSize_lineEdit)
        # Submit Button
        self.submit_pushButton = QPushButton(self.titleBarGameWidget, clicked= lambda:self.gameLogic.areYouSureSubmit())
#         self.submit_pushButton.setStyleSheet("QPushButton:focus { border: 2px solid blue; border-radius: 5px; outline: none; }")
        self.submit_pushButton.setObjectName("submit_pushButton")
        self.titleBarGame.addWidget(self.submit_pushButton)
        # Color mode
        self.paletteMode_comboBox = QComboBox(self)
        self.colorModes = ["Light", "Dark", "Contrast"]
        self.paletteMode_comboBox.addItems(self.colorModes)
        self.titleBarGame.addWidget(self.paletteMode_comboBox)
        # Color modes
        self.palette = QPalette()
        self.defaultPalette = MainWindow.palette()
        self.paletteMode_comboBox.currentTextChanged.connect(self.apply_selected_mode)



        # Save game
        self.saveGame_pushButton = QPushButton(self.titleBarGameWidget, clicked = lambda:self.gameLogic.areYouSureSave())
        self.saveGame_pushButton.setObjectName("saveGame_pushButton")
#         self.saveGame_pushButton.setStyleSheet("QPushButton:focus { border: 2px solid blue; border-radius: 5px; outline: none; }")
        
        self.titleBarGame.addWidget(self.saveGame_pushButton)

        # Load game
        self.loadGame_pushButton = QPushButton(self.titleBarGameWidget, clicked = lambda:self.gameLogic.areYouSureLoad())
        self.loadGame_pushButton.setObjectName("loadGame_pushButton")
#         self.loadGame_pushButton.setStyleSheet("QPushButton:focus { border: 2px solid blue; border-radius: 5px; outline: none; }")
        self.titleBarGame.addWidget(self.loadGame_pushButton)

        # Turns
        self.currentTurnBoxWidget = QWidget(self.centralwidget)
        self.currentTurnBox = QHBoxLayout(self.currentTurnBoxWidget)
        self.appBoardLayout.addWidget(self.currentTurnBoxWidget, 4, 0, 1, 5)
        # Position: Row 4, Column 0. Span: 5 rows, 1 column
        
        self.currentTurnBoxWidget.setObjectName("currentTurnBoxWidget")
        self.currentTurnBox.setObjectName("currentTurnBox")

        self.playterTurn_label = QLabel(self.currentTurnBoxWidget)
        self.currentTurnBox.addWidget(self.playterTurn_label)
        self.currentTurnBox.setAlignment(self.playterTurn_label, QtCore.Qt.AlignCenter)
        
        font = QFont()
        font.setPointSize(20)
        self.playterTurn_label.setFont(font)
        

        
        self.playterTurn_label.setObjectName("playterTurn_label")
        MainWindow.setCentralWidget(self.centralwidget)
        
        
       
        self.gameLogic = GameLogic(self)
        
        MainWindow.resizeEvent = self.resizeEvent

        #Board Initializing
        self.buildTheBoard()
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.simpleGame_radioButton.clicked.connect(self.areYouSureGameMode)
        self.generalGame_radioButton.clicked.connect(self.areYouSureGameMode)
        
        
        
        # Set Tab Order
        MainWindow.setTabOrder(self.simpleGame_radioButton, self.generalGame_radioButton)
        MainWindow.setTabOrder(self.generalGame_radioButton, self.boardSize_lineEdit)
        MainWindow.setTabOrder(self.boardSize_lineEdit, self.submit_pushButton)
        MainWindow.setTabOrder(self.submit_pushButton, self.saveGame_pushButton)
        MainWindow.setTabOrder(self.paletteMode_comboBox, self.saveGame_pushButton)
        MainWindow.setTabOrder(self.saveGame_pushButton, self.loadGame_pushButton)
        MainWindow.setTabOrder(self.loadGame_pushButton, self.blueComputerPlayer)
        MainWindow.setTabOrder(self.blueComputerPlayer, self.bluePlayerS_radioButton)
        MainWindow.setTabOrder(self.bluePlayerS_radioButton, self.bluePlayerO_radioButton)
        MainWindow.setTabOrder(self.bluePlayerO_radioButton, self.redComputerPlayer)
        MainWindow.setTabOrder(self.redComputerPlayer, self.redPlayerS_radioButton)
        MainWindow.setTabOrder(self.redPlayerS_radioButton, self.redPlayerO_radioButton)
        MainWindow.setTabOrder(self.redPlayerO_radioButton, self.buttons[(0,0)])
        
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Up and event.modifiers() & Qt.ShiftModifier:
            self.changePlayerUp()
        elif event.key() == Qt.Key_Down and event.modifiers() & Qt.ShiftModifier:
            self.changePlayerDown()
        super(MyMainWindow, self).keyPressEvent(event)
        
#         MainWindow.setTabOrder(self.buttons[(0,0)], self.simpleGame_radioButton )
        
        
#       Debugging
#         self.print_window_size()



#     def focusNextPrevChild(self, next_child):
#         if isinstance(self.focusWidget(), FocusButton):
#             return False
#         return super(Ui_MainWindow, self).focusNextPrevChild(next_child)

    def resizeEvent(self, event):
        rows = int(self.boardSize_lineEdit.text())
        columns = int(self.boardSize_lineEdit.text())
        max_button_size = int(min(self.MainWindow.width() / (columns*2), self.MainWindow.height() / (rows*1.5)))
        font = QFont()
        font.setPointSize(int(max_button_size//2))
        for row in range(rows):
            for col in range(columns):
                self.buttons[(row, col)].setFixedSize(max_button_size, max_button_size)
                self.buttons[(row, col)].setFont(font)
        
        playerLabel_fontSize = int(min(self.MainWindow.width() / 35, self.MainWindow.height() / 25))
        computerPlayer_fontSize = int(min(self.MainWindow.width() / 40, self.MainWindow.height() / 30))
        matches_fontSize = int(min(self.MainWindow.width() / 55, self.MainWindow.height() / 40))
        radioButton_fontSize = int(min(self.MainWindow.width() / 25, self.MainWindow.height() / 15))
        button_pixelSize = int(min(self.MainWindow.width() / 30, self.MainWindow.height() / 20))
        
#         print(f"Min of both: {int(min(MainWindow.width() / 25, MainWindow.height() / 25))}, WindowWidth/25:{MainWindow.width() / 25}, WindowHeight/25: {MainWindow.height() / 25} ")
        
        PlayerLabelFont = QFont()
        PlayerLabelFont.setPointSize(playerLabel_fontSize)
        
        PlayerComputerFont = QFont()
        PlayerComputerFont.setPointSize(computerPlayer_fontSize)
        
        PlayerMatchesFont = QFont()
        PlayerMatchesFont.setPointSize(matches_fontSize)
        
        PlayerButtonsFont = QFont()
        PlayerButtonsFont.setPointSize(radioButton_fontSize)
        
        
#         self.bluePlayerS_radioButton.setStyleSheet(f"QRadioButton::indicator {{ width:{button_pixelSize}px; height: {button_pixelSize}px;}}")
        
        self.bluePlayer_abel.setFont(PlayerLabelFont)
        self.blueComputerPlayer.setFont(PlayerComputerFont)
        self.bluePlayerMatches_label.setFont(PlayerMatchesFont)
        self.bluePlayerS_radioButton.setFont(PlayerButtonsFont)
        self.bluePlayerO_radioButton.setFont(PlayerButtonsFont)
        
        self.redPlayer_label.setFont(PlayerLabelFont)
        self.redComputerPlayer.setFont(PlayerComputerFont)
        self.redPlayerMatches_label.setFont(PlayerMatchesFont)
        self.redPlayerS_radioButton.setFont(PlayerButtonsFont)
        self.redPlayerO_radioButton.setFont(PlayerButtonsFont)
        


    # Method to apply selected mode
    def apply_selected_mode(self):
        selected_mode = str(self.paletteMode_comboBox.currentText())
        if selected_mode == 'Dark':
            self.toggle_dark_mode()
        elif selected_mode == 'Light':
            self.toggle_light_mode()
        elif selected_mode == 'Contrast':
            self.toggle_contrast_mode()
            
        
    def toggle_dark_mode(self):
        dark_palette = QPalette()
        # Avoiding pure black surfaces
        dark_palette.setColor(QPalette.Window, QColor(30, 30, 30))  # Slightly off-black background
        dark_palette.setColor(QPalette.Button, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.Highlight, QColor(142, 45, 197).lighter())
        # Light text, but not pure white
        dark_palette.setColor(QPalette.ButtonText, QColor(200, 200, 200))
        dark_palette.setColor(QPalette.WindowText, QColor(200, 200, 200))
        dark_palette.setColor(QPalette.BrightText, QColor(255, 255, 255))
        dark_palette.setColor(QPalette.Link, QColor(42, 130, 218))
        # Making sure browser focus indicators are visible
        dark_palette.setColor(QPalette.HighlightedText, QColor(0, 0, 0))
        # Anticipating potential issues with astigmatism, keeping contrasts moderate 
        dark_palette.setColor(QPalette.Disabled, QPalette.Text, QColor(127, 127, 127))
#         dark_palette.setColor(QPalette.Disabled, QPalette.Button, QColor(63, 63, 63))
#         dark_palette.setColor(QPalette.Disabled, QPalette.ButtonText, QColor(127, 127, 127))

        self.MainWindow.setPalette(dark_palette)
    
        # Assuming self.buttons is a list of all your QPushButton instances
        with open(r"CSS/darkButtons.json", 'r') as file:
            style = json.load(file)
            for row in range(self.rows):
                for col in range(self.columns):
                    self.buttons[(row, col)].setStyleSheet("")
                    self.buttons[(row, col)].setStyleSheet(style['button_style'])

        
    def toggle_light_mode(self):
        self.MainWindow.setPalette(self.defaultPalette)
        with open(r"CSS/lightButtons.json", 'r') as file:
            style = json.load(file)
            for row in range(self.rows):
                for col in range(self.columns):
                    self.buttons[(row, col)].setStyleSheet("")
                    # self.buttons[(row, col)].setStyleSheet(style['button_style'])

        
    def toggle_contrast_mode(self):
        contrast_palette = QPalette()
        contrast_palette.setColor(QPalette.Window, QColor(0, 0, 0)) # Black background
        contrast_palette.setColor(QPalette.Button, QColor(255, 255, 255)) # White buttons
        contrast_palette.setColor(QPalette.Highlight, QColor(255, 255, 255)) # White highlight
        contrast_palette.setColor(QPalette.ButtonText, QColor(0, 0, 0)) # Black button text
        contrast_palette.setColor(QPalette.WindowText, QColor(255, 255, 0)) # White text
        contrast_palette.setColor(QPalette.BrightText, QColor(255, 255, 0)) # Bright yellow for bright text
        contrast_palette.setColor(QPalette.Link, QColor(0, 0, 255)) # Blue links
        contrast_palette.setColor(QPalette.HighlightedText, QColor(0, 0, 0)) # Black highlighted text
        self.MainWindow.setPalette(contrast_palette)
        with open(r"CSS/contrastButtons.json", 'r') as file:
            style = json.load(file)
            for row in range(self.rows):
                for col in range(self.columns):
                    # self.buttons[(row, col)].setStyleSheet("")
                    if  not self.buttons[(row, col)].winningButton: self.buttons[(row, col)].setStyleSheet(style['button_style'])
        with open(r"CSS/win_contrastButtons.json", 'r') as file:
            style = json.load(file)
            for row in range(self.rows):
                for col in range(self.columns):
                    # self.buttons[(row, col)].setStyleSheet("")
                    if  self.buttons[(row, col)].winningButton: self.buttons[(row, col)].setStyleSheet(style['button_style']) 
                


        

# Builds the playable game area for the SoS game
    def buildTheBoard(self):
        if self.gameLogic.acceptableBoard():
            self.gameLogic.resetBoard()
             # Remove any existing buttons from the layout
            for i in reversed(range(self.gamePlayLayout.count())):
                widget = self.gamePlayLayout.itemAt(i).widget()
                self.gamePlayLayout.removeWidget(widget)
                widget.setParent(None)

            # Create new buttons
            self.buttons = {}
            self.rows = int(self.boardSize_lineEdit.text())
            self.columns = int(self.boardSize_lineEdit.text())
            max_button_size = int(min(self.MainWindow.width() / (self.columns*2), self.MainWindow.height() / (self.rows*1.5)))
            font = QFont()
            font.setPointSize(int(max_button_size//2))
            for row in range(self.rows):
                for col in range(self.columns):
                    self.buttons[(row, col)] = FocusButton('', self)
                    self.buttons[(row, col)] = FocusButton((row, col), self)
                    self.buttons[(row, col)].setFixedSize(max_button_size, max_button_size)
                    self.buttons[(row, col)].clicked.connect(self.gameLogic.buttonClicked)
                    self.buttons[(row, col)].setFont(font)
                    self.buttons[(row, col)].winningButton = False
                    self.gamePlayLayout.addWidget(self.buttons[(row, col)], row, col)
                    self.buttons[(row, col)].setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            # Set flag to indicate game mode
            if self.simpleGame_radioButton.isChecked():
                self.simple_game = True
                self.general_game = False
            elif self.generalGame_radioButton.isChecked():
                self.general_game = True
                self.simple_game = False

            if str(self.paletteMode_comboBox.currentText()) == "Dark":
                self.toggle_dark_mode()
            elif str(self.paletteMode_comboBox.currentText()) == "Light":
                self.toggle_light_mode()
            elif str(self.paletteMode_comboBox.currentText()) == "Contrast":
                self.toggle_contrast_mode()
                
                


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.bluePlayer_abel.setText(_translate("MainWindow", "Blue Player"))
        self.blueComputerPlayer.setText(_translate("MainWindow", "Computer"))
        self.bluePlayerS_radioButton.setText(_translate("MainWindow", "S"))
        self.bluePlayerO_radioButton.setText(_translate("MainWindow", "O"))
        self.redPlayer_label.setText(_translate("MainWindow", "Red Player"))
        self.redComputerPlayer.setText(_translate("MainWindow", "Computer"))
        self.redPlayerS_radioButton.setText(_translate("MainWindow", "S"))
        self.redPlayerO_radioButton.setText(_translate("MainWindow", "O"))
        self.gameName_label.setText(_translate("MainWindow", "SOS"))
        self.simpleGame_radioButton.setText(_translate("MainWindow", "Simple game"))
        self.generalGame_radioButton.setText(_translate("MainWindow", "General game"))
        self.boardSize_label.setText(_translate("MainWindow", "Board Size"))
        self.submit_pushButton.setText(_translate("MainWindow", "Submit"))
        self.playterTurn_label.setText(_translate("MainWindow", "Current Turn: blue"))
        self.saveGame_pushButton.setText(_translate("MainWindow", "Save Game"))
        self.loadGame_pushButton.setText(_translate("MainWindow", "Load Game"))

#   Debugging
    def print_window_size(self):
        QTimer.singleShot(5000, self.print_window_size)
        print(f' Main Window Width: {MainWindow.size().width()}, Main Window Height: {MainWindow.size().height()}')
        

    def areYouSureGameMode(self):
        confirm_box = QMessageBox()
        confirm_box.setIcon(QMessageBox.Question)
        confirm_box.setWindowTitle("Changing Game Mode")
        confirm_box.setText("Changing the game mode will create a new board and clear the current game. Are you sure you want to do this?")
        confirm_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        confirm_box.setDefaultButton(QMessageBox.No)
        reply = confirm_box.exec_()
        if reply == QMessageBox.Yes:
            if self.sender() == self.simpleGame_radioButton:
                self.buildTheBoard()
            if self.sender() == self.generalGame_radioButton:
                self.buildTheBoard()
        else:
            if self.simpleGame_radioButton.isChecked():
                self.simpleGame_radioButton.setChecked(False)
                self.generalGame_radioButton.setChecked(True)
            else:
                self.simpleGame_radioButton.setChecked(True)
                self.generalGame_radioButton.setChecked(False)
                
                
    def changePlayerUp(self):
        print("Shift+Up")
        label_text = str(self.playterTurn_label.text())
        if 'blue' in label_text:
            self.bluePlayerS_radioButton.setChecked(
            not self.bluePlayerS_radioButton.isChecked())
        elif 'red' in label_text:
            self.redPlayerS_radioButton.setChecked(
            not self.redPlayerS_radioButton.isChecked())
            
    def changePlayerDown(self):
        label_text = str(self.playterTurn_label.text())
        if 'blue' in label_text:
            self.bluePlayerO_radioButton.setChecked(
            not self.bluePlayerO_radioButton.isChecked())
        elif 'red' in label_text:
            self.redPlayerO_radioButton.setChecked(
            not self.redPlayerO_radioButton.isChecked())


