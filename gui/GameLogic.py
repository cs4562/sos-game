from PyQt5.QtWidgets import (
    QMessageBox,
    QCheckBox,
    QApplication,
)
from PyQt5 import QtCore
import random

class GameLogic:
    
    def __init__(self, ui_window):
        self.ui_window = ui_window
        self.chkBoxState = {
            "Submit": True,
            "Load": True,
            "Save": True,
        }
        
    # Computer making a move on the Board
    def makeMove(self, color):
        available_moves =  self.getAvailableMoves()
        game_moves = ["S", "O"]
        current_turn = self.ui_window.playterTurn_label.text().split()[-1]
        if color == current_turn:
            if not available_moves:
                return
            else:
                QtCore.QTimer.singleShot(500, lambda: self.makeComputerMove(current_turn, available_moves, game_moves))
        else:
            return

    # Logic for Computer turn
    def makeComputerMove(self, current_turn, available_moves, game_moves):
        move = random.choice(available_moves)
        SorO = random.choice(game_moves)
        self.ui_window.buttons[(move[0], move[1])].setText(SorO)
        self.ui_window.buttons[(move[0], move[1])].setStyleSheet("color: #bebebe")
        self.ui_window.buttons[(move[0], move[1])].setEnabled(False)
        if current_turn == "blue":
            self.disableBlueSide()
            self.ui_window.playterTurn_label.setText("Current Turn: red")
            self.checkWin("blue")
            self.ui_window.bluePlayerMatches_label.setText(f"Blue Matches: {self.ui_window.bluePlayerMatches}")
            if self.ui_window.redComputerPlayer.isChecked():
                self.makeMove("red")
        else:
            self.disableRedSide()
            self.ui_window.playterTurn_label.setText("Current Turn: blue")
            self.checkWin("red")
            self.ui_window.redPlayerMatches_label.setText(f"Red Matches: {self.ui_window.redPlayerMatches}")
            if self.ui_window.blueComputerPlayer.isChecked():
                self.makeMove("blue")

    # Returns a list of available moves
    def getAvailableMoves(self):
        available_moves = []
        rows = int(self.ui_window.boardSize_lineEdit.text())
        columns = int(self.ui_window.boardSize_lineEdit.text())
        for row in range(rows):
            for col in range(columns):
                if self.ui_window.buttons[(row, col)].text() == "":
                    available_moves.append((row, col))
        return available_moves


    # If a general game is being played this function will compare the match score and display
    # a message to the user saying who won
    def generalGameWin(self):
        if self.ui_window.general_game or self.ui_window.simple_game:
            if self.allButtonsClicked():
                if self.ui_window.bluePlayerMatches > self.ui_window.redPlayerMatches:
                    message = f"Blue Wins!! Blue scored {self.ui_window.bluePlayerMatches} matches."
                elif self.ui_window.redPlayerMatches > self.ui_window.bluePlayerMatches:
                    message = f"Red Wins!! Red scored {self.ui_window.redPlayerMatches} matches."
                else:
                    message = "Tie Game"
                self.showMessageBox(message)



    # Checks if all buttons have been pressed
    def allButtonsClicked(self):
        for button in self.ui_window.buttons.values():
            if button.isEnabled():
                return False
        return True

    # Handles creating a message box to display winners
    def showMessageBox(self, message):
        msg = QMessageBox()
        msg.setText(message)
        msg.setWindowTitle("Game Over")
        msg.setStandardButtons(msg.Ok)

        showMsg = msg.exec_()
        if showMsg == QMessageBox.Ok:
            self.ui_window.buildTheBoard()

    # Whenever a match is made this function will be called and if a simple game
    # a message will display saying whoever made the match won the game
    def simpleGameWin(self, currentTurn):
        if self.ui_window.simple_game:
            message = f"{currentTurn.capitalize()} has won the game!"
            self.showMessageBox(message)
        else:
            return


    





    # Checks to make sure the user inputted appropriate data to construct the board.
    def acceptableBoard(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setWindowTitle("Invalid Input")
        msg.setStandardButtons(msg.Ok)
        try:
            if int(self.ui_window.boardSize_lineEdit.text()) < 3:
                msg.setText(f"{self.ui_window.boardSize_lineEdit.text()} is too small. Please insert a number greater or equal to 3" )
                showMsg = msg.exec_()
                if showMsg == msg.Ok:
                    return False
            elif int(self.ui_window.boardSize_lineEdit.text()) > 9:
                msg.setText(f"{self.ui_window.boardSize_lineEdit.text()} is too large. Please insert a number less than 9")
                showMsg = msg.exec_()
                if showMsg == msg.Ok:
                    return False
        except ValueError:
                msg.setText(f"{self.ui_window.boardSize_lineEdit.text()} was inputted and that is not a valid number")
                showMsg = msg.exec_()
                if showMsg == msg.Ok:
                    return False
        return True

    # Disables the Red Players buttons
    def disableRedSide(self):
        self.ui_window.bluePlayerO_radioButton.setEnabled(True)
        self.ui_window.bluePlayerS_radioButton.setEnabled(True)
        self.ui_window.redPlayerO_radioButton.setEnabled(False)
        self.ui_window.redPlayerS_radioButton.setEnabled(False)

        # Disables the Blue Players buttons
    def disableBlueSide(self):
        self.ui_window.redPlayerS_radioButton.setEnabled(True)
        self.ui_window.redPlayerO_radioButton.setEnabled(True)
        self.ui_window.bluePlayerS_radioButton.setEnabled(False)
        self.ui_window.bluePlayerO_radioButton.setEnabled(False)

        # Sets default values to the game board
    def resetBoard(self):
        self.ui_window.bluePlayerO_radioButton.setEnabled(True)
        self.ui_window.bluePlayerS_radioButton.setEnabled(True)
        self.ui_window.redPlayerO_radioButton.setEnabled(True)
        self.ui_window.redPlayerS_radioButton.setEnabled(True)
        self.ui_window.bluePlayerS_radioButton.setChecked(True)
        self.ui_window.redPlayerS_radioButton.setChecked(True)
        self.ui_window.playterTurn_label.setText("Current Turn: blue")
        self.ui_window.redPlayerMatches = 0
        self.ui_window.redPlayerMatches_label.setText(f"Red Matches: {self.ui_window.redPlayerMatches}")
        self.ui_window.bluePlayerMatches = 0
        self.ui_window.bluePlayerMatches_label.setText(f"Blue Matches: {self.ui_window.bluePlayerMatches}")


    # Whenever a user clicks a button on the board this function will check who's turnit is and if the user wants to place a S or an O
    # on the board. It will will check to see if the user has made any matches or has won the game
    def buttonClicked(self):
        sender = self.ui_window.sender()

        # Get the current turn from the playerTurn_label
        current_turn = self.ui_window.playterTurn_label.text().split()[-1]

        # Disable other player buttons and sets button
        if current_turn == "blue":
            self.disableBlueSide()
            for (row, col), button in self.ui_window.buttons.items():
                if sender == button:
                    if self.ui_window.bluePlayerO_radioButton.isChecked():
                        button.setText("O")
#                         button.setStyleSheet("color: #bebebe")
                        if button.wasClicked: button.clearFocus()
                        button.setEnabled(False)
                    elif self.ui_window.bluePlayerS_radioButton.isChecked():
                        button.setText("S")
#                         button.setStyleSheet("color: #bebebe")
                        if button.wasClicked: button.clearFocus()
                        button.setEnabled(False)
                    elif not (self.ui_window.redPlayerO_radioButton.isChecked()
                            or self.ui_window.redPlayerS_radioButton.isChecked()):
                        return
            self.ui_window.playterTurn_label.setText("Current Turn: red")
            self.checkWin("blue")
            self.ui_window.bluePlayerMatches_label.setText(f"Blue Matches: {self.ui_window.bluePlayerMatches}")
            if self.ui_window.redComputerPlayer.isChecked():
                self.makeMove("red")
        else:
            self.disableRedSide()
            self.ui_window.playterTurn_label.setText("Current Turn: blue")
            for (row, col), button in self.ui_window.buttons.items():
                if sender == button:
                    if self.ui_window.redPlayerO_radioButton.isChecked():
                        button.setText("O")
#                         button.setStyleSheet("color: #bebebe")
                        if button.wasClicked: button.clearFocus()
                        button.setEnabled(False)
                    elif self.ui_window.redPlayerS_radioButton.isChecked():
                        button.setText("S")
#                         button.setStyleSheet("color: #bebebe")
                        if button.wasClicked: button.clearFocus()
                        button.setEnabled(False)
                    else:
                        return

                        
            self.ui_window.playterTurn_label.setText("Current Turn: blue")
            self.checkWin("red")
            self.ui_window.redPlayerMatches_label.setText(f"Red Matches: {self.ui_window.redPlayerMatches}")
            if self.ui_window.blueComputerPlayer.isChecked():
                self.makeMove("blue")
        



    # Checks for matches in a Diagonal, Horizontal, or Vertical pattern and checks to see if the user has won the game
    def checkWin(self, currentTurn):
        rows = self.ui_window.rows
        columns = self.ui_window.columns
        # Check Rows
        for row in range(rows):
            for col in range(columns-2):
                # Check Horizontal
                if (self.ui_window.buttons[(row, col)].text() == "S"
                                 and self.ui_window.buttons[(row, col+1)].text() == "O"
                                 and self.ui_window.buttons[(row, col+2)].text() == "S"):
                    self.countMatch(self.ui_window.buttons[(row, col)], self.ui_window.buttons[(row, col+1)], self.ui_window.buttons[(row, col+2)], currentTurn)
                    self.changeColors(self.ui_window.buttons[(row, col)], self.ui_window.buttons[(row, col+1)], self.ui_window.buttons[(row, col+2)], currentTurn)
                    self.simpleGameWin(currentTurn)

        # Check Vertical
        for col in range(columns):
            for row in range(rows-2):
                if (self.ui_window.buttons[(row, col)].text() == "S"
                                 and self.ui_window.buttons[(row+1, col)].text() == "O"
                                 and self.ui_window.buttons[(row+2, col)].text() == "S"):
                    self.countMatch(self.ui_window.buttons[(row, col)], self.ui_window.buttons[(row+1, col)], self.ui_window.buttons[(row+2, col)], currentTurn)
                    self.changeColors(self.ui_window.buttons[(row, col)], self.ui_window.buttons[(row+1, col)], self.ui_window.buttons[(row+2, col)], currentTurn)
                    self.simpleGameWin(currentTurn)

        # Check Diagonal
        for row in range(rows-2):
            for col in range(columns-2):
                if (self.ui_window.buttons[(row, col)].text() == "S"
                                 and self.ui_window.buttons[(row+1, col+1)].text() == "O"
                                 and self.ui_window.buttons[(row+2, col+2)].text() == "S"):
                    self.countMatch(self.ui_window.buttons[(row, col)], self.ui_window.buttons[(row+1, col+1)], self.ui_window.buttons[(row+2, col+2)], currentTurn)
                    self.changeColors(self.ui_window.buttons[(row, col)], self.ui_window.buttons[(row+1, col+1)], self.ui_window.buttons[(row+2, col+2)], currentTurn)
                    self.simpleGameWin(currentTurn)

        for row in range(rows-2):
            for col in range(columns-2):
                if (self.ui_window.buttons[(row, col+2)].text() == "S"
                                 and self.ui_window.buttons[(row+1, col+1)].text() == "O"
                                 and self.ui_window.buttons[(row+2, col)].text() == "S"):
                    self.countMatch(self.ui_window.buttons[(row, col+2)], self.ui_window.buttons[(row+1, col+1)], self.ui_window.buttons[(row+2, col)], currentTurn)
                    self.changeColors(self.ui_window.buttons[(row, col+2)], self.ui_window.buttons[(row+1, col+1)], self.ui_window.buttons[(row+2, col)], currentTurn)
                    self.simpleGameWin(currentTurn)
        self.generalGameWin()


    # The function will check if the matching buttons are new and count the new match
    def countMatch(self, button1, button2, button3, currentTurn):
        if button1.winningButton != True and currentTurn  == 'red':
            self.ui_window.redPlayerMatches += 1
            return
        elif button1.winningButton != True and currentTurn == 'blue':
            self.ui_window.bluePlayerMatches +=1
            return

        if button2.winningButton != True and currentTurn  == 'red':
            self.ui_window.redPlayerMatches += 1
            return
        elif button2.winningButton != True and currentTurn == 'blue':
            self.ui_window.bluePlayerMatches +=1
            return

        if button3.winningButton != True and currentTurn  == 'red':
            self.ui_window.redPlayerMatches += 1
            return
        elif button3.winningButton != True and currentTurn == 'blue':
            self.ui_window.bluePlayerMatches +=1
            return

    # Will change the colors to reflect who made the match and set the winningButton flag to true
    def changeColors(self, a, b, c, currentTurn):
        if a.winningButton != True:
            a.setStyleSheet(f'color: {currentTurn}')
        if b.winningButton != True:
            b.setStyleSheet(f'color: {currentTurn}')
        if c.winningButton != True:
            c.setStyleSheet(f'color: {currentTurn}')
        a.winningButton = True
        b.winningButton = True
        c.winningButton = True

    # Saves the game state to a json file
    def saveGame(self):
            game_state = {
                "board_size": int(self.ui_window.boardSize_lineEdit.text()),
                "board": {},
                "blue_matches": self.ui_window.bluePlayerMatches,
                "red_matches": self.ui_window.redPlayerMatches,
                "game_mode": "simple" if self.ui_window.simpleGame_radioButton.isChecked() else "general",
                "current_turn": self.ui_window.playterTurn_label.text().split()[-1],
            }

            for (row, col), button in self.ui_window.buttons.items():
                key = f"{row}, {col}"
                game_state["board"][key] = {
                    "text": button.text(),
                    "color": button.palette().buttonText().color().name()
                }



            with open("gameSaves/saved_game.json", "w") as save_file:
                json.dump(game_state, save_file)
                
            QMessageBox.information(QApplication.activeWindow(), "Save Game", "Game saved successfully.")
            

    # Loads game from json file and builds the board
    def loadGame(self):
        try:
            with open("gameSaves/saved_game.json", "r") as load_file:
                game_state = json.load(load_file)


            # Ensuring the boardSize is correct
            saved_boardSize = game_state["board_size"]
            current_boardSize = int(self.ui_window.boardSize_lineEdit.text())

            if saved_boardSize != current_boardSize:
                self.ui_window.boardSize_lineEdit.setText(str(saved_boardSize))
                self.ui_window.buildTheBoard()


            self.ui_window.bluePlayerMatches = game_state["blue_matches"]
            self.ui_window.redPlayerMatches = game_state["red_matches"]

            self.ui_window.bluePlayerMatches_label.setText(f"Blue Matches: {self.ui_window.bluePlayerMatches}")
            self.ui_window.redPlayerMatches_label.setText(f"Red Matches: {self.ui_window.redPlayerMatches}")


            # Ensuring the game is in the correct mode
            game_mode = game_state["game_mode"]
            if game_mode == "simple":
                self.ui_window.simpleGame_radioButton.setChecked(True)
                self.ui_window.simple_game = True
                self.ui_window.general_game = False
            elif game_mode == "general":
                self.ui_window.generalGame_radioButton.setChecked(True)
                self.ui_window.general_game = True
                self.ui_window.simple_game = False

            # Ensuring the the game kept the same turn
            current_turn = game_state["current_turn"]
            self.ui_window.playterTurn_label.setText(f'Current Turn: {current_turn}')

            if current_turn == "blue":
                self.disableRedSide()
            else:
                self.disableBlueSide()

            # Building the gameBoard
            for key, button_data in game_state["board"].items():
                row, col = map(int, key.split(','))
                button = self.ui_window.buttons[(row, col)]
                button.setText(button_data["text"])
                button.setEnabled(True)

                # set button color if it's necessary
                color = button_data["color"]
                button.setStyleSheet(f"color: {color}")

                # Disable the buttons that have been played
                if button_data["text"]:
                    if button_data["color"] == '#ff0000' or button_data["color"] == '#0000ff':
                        button.winningButton = True
                    else:
                        button.setStyleSheet("color: #bebebe")
                    button.setEnabled(False)

        except FileNotFoundError:
            QMessageBox.information(QApplication.activeWindow(), "Load Game", "No saved game found.")
            
        except Exception as e:
            QMessageBox.information(QApplication.activeWindow(), "Load Game", e)

        QMessageBox.information(QApplication.activeWindow(), "Load Game", "Game loaded successfully.")
            
            
    def areYouSureGen(self, title, message, exec_func, chkBoxType):
        if self.chkBoxState[chkBoxType]:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Question)
            msg.setText(message)
            dontSeeMeAgain = QCheckBox("Click me if you don't want to see this window again")
            dontSeeMeAgain.stateChanged.connect(lambda: self.areYouSureUpdateChkBoxState(dontSeeMeAgain, chkBoxType))
            msg.setCheckBox(dontSeeMeAgain)
            msg.setWindowTitle(title)
            msg.setStandardButtons(msg.Yes | msg.No)
            reply = msg.exec_()
            if reply == QMessageBox.Yes:
                exec_func()
        else:
            exec_func()

            
    
    # Verification prompt will be displayed whenever the user requests to create a new board
    def areYouSureUpdateChkBoxState(self, checkbox, chkBoxType):
        if checkbox.isChecked():
            self.chkBoxState[chkBoxType] = False
            
    def areYouSureSubmit(self):
        msg = "Pressing submit will create a new board and clear the current game. Are you sure you want to do this?"
        self.areYouSureGen("Are you sure?", msg, self.ui_window.buildTheBoard, "Submit")
        
    def areYouSureLoad(self):
        msg = "Are you sure you want to load a previous game? All current progress will be lost."
        self.areYouSureGen("Load Game", msg, self.loadGame, "Load")

        
    def areYouSureSave(self):
        msg = "This will overwrite your current save. Are you sure you want to save the game?"
        self.areYouSureGen("Save Game", msg, self.saveGame, "Save")
