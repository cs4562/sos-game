import mainwin1 as mainwin
import sys
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
)

app = QApplication(sys.argv)
MainWindow = QMainWindow()
ui = mainwin.Ui_MainWindow()
ui.setupUi(MainWindow)
MainWindow.show()
sys.exit(app.exec_())
