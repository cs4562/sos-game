import json
def create_stylesheet():
    # Store stylesheet into the file
    darkButton_style = """
        FocusButton {
            background-color: #353535;
            color: #F0F0F0;
            border-radius: 10px;
            padding: 5px;
        }
        FocusButton::pressed {
            background-color: #2D2D2D;
        }
        FocusButton {
            border: 2px solid #232323;
            border-radius: 10px;
            color: white;
            background-color: #353535;
            padding: 5px;
        }
        FocusButton:hover {
            border: 2px solid #707070;
            border-radius: 10px;
            padding: 5px;
        }
        FocusButton:pressed {
            border: 2px solid #232323;
            border-radius: 10px;
            padding: 5px;
        }
        FocusButton:disabled {
            background-color: #555555;
            color: #a0a0a0;
            border: 2px solid #444444;
        }
        FocusButton:focus {
            border: 2px solid blue;
            border-radius: 5px;
            outline: none;
        }
    """
    lightButton_style = """
        FocusButton {
            background-color: #f0f0f0;
            color: #000000;
            border-radius: 4px;
            padding: 5px;
            border: 2px solid #7b7b7b;
            transition: all 0.3s ease-in-out;
        }
        FocusButton:hover {
            border: 2px solid #5e5e5e;
        }
        FocusButton:active {
            border: 2px solid #5e5e5e;
            background-color: #e2e2e2;
        }
        FocusButton:disabled {
            background-color: #f0f0f0;
            color: #a0a0a0;
            border: 2px solid #f0f0f0;
        }
        FocusButton:focus {
            border: 2px solid #4463A6;
            outline: none;
        }
    """
    
    contrastButton_style = """
        FocusButton {
            background-color: #000000;
            color: #FFFFFF;
            border-radius: 10px;
            padding: 5px;
        }
        FocusButton::pressed {
            background-color: #FFFFFF;
            color: #000000;
        }
        FocusButton {
            border: 2px solid #FFFFFF;
            border-radius: 10px;
            color: white;
            background-color: #000000;
            padding: 5px;
        }
        FocusButton:hover {
            border: 2px solid #808080;
            border-radius: 10px;
            padding: 5px;
        }
        FocusButton:pressed {
            border: 2px solid #FFFFFF;
            border-radius: 10px;
            padding: 5px;
        }
        FocusButton:disabled {
            background-color: #000000;
            color: yellow;
            border: 2px solid #444444;
        }
        FocusButton:focus {
            border: 2px solid blue;
            border-radius: 5px;
            outline: none;
        }
    """
    with open(r"CSS/darkButtons.json", 'w') as file:
        json.dump({'button_style': darkButton_style}, file)
    with open(r"CSS/lightButtons.json", 'w') as file:
        json.dump({'button_style': lightButton_style}, file)
    with open(r"CSS/contrastButtons.json", 'w') as file:
        json.dump({'button_style': contrastButton_style}, file)

create_stylesheet()
    