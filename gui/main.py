from mainWindow import Ui_MainWindow
import sys
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
)

app = QApplication(sys.argv)
MainWindow = QMainWindow()
ui = Ui_MainWindow()
ui.setupUi(MainWindow)
MainWindow.show()
sys.exit(app.exec_())
