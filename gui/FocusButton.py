from PyQt5.QtWidgets import QPushButton, QApplication
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5 import QtCore

# Create a new button class that inherits from QPushButton
class FocusButton(QPushButton):
    # This signal is emitted when the button gets focus
    focusReceived = pyqtSignal()

    def __init__(self, grid_position, ui_window, *args):
        super().__init__(*args)
        self.grid_position = grid_position
        self.ui_window = ui_window
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        
        self.wasClicked = False
        QApplication.instance().installEventFilter(self)
        
    def eventFilter(self, obj, event):
        # Check if obj is an instance of FocusButton
        if isinstance(obj, FocusButton):
            if event.type() == QtCore.QEvent.KeyPress:
                key = event.key()
                if key == QtCore.Qt.Key_Tab:
                    self.ui_window.simpleGame_radioButton.setFocus()
                    return True
        return super().eventFilter(obj, event)
        
    # Override the focusInEvent to emit your own signal
    def focusInEvent(self, QFocusEvent):
        self.focusReceived.emit()
        super().focusInEvent(QFocusEvent)
        
    def mousePressEvent(self, event):
        super().mousePressEvent(event)
        self.wasClicked = True

        
    def keyPressEvent(self, event):
        row, col = self.grid_position
        max_row, max_col = self.ui_window.rows, self.ui_window.columns
        grid_size = (max_row, max_col)
            
        if event.key() == QtCore.Qt.Key_Up:
            new_row = row - 1
            while new_row >= 0:
                new_button = self.ui_window.buttons[(new_row, col)]
                if new_button.isEnabled():
                    new_button.setFocus()
                    return
                new_row -= 1
        elif event.key() == QtCore.Qt.Key_Down:
            new_row = row + 1
            while new_row < max_row:
                new_button = self.ui_window.buttons[(new_row, col)]
                if new_button.isEnabled():
                    new_button.setFocus()
                    return
                new_row += 1
        elif event.key() == QtCore.Qt.Key_Left:
            new_col = col - 1
            while new_col >= 0:
                new_button = self.ui_window.buttons[(row, new_col)]
                if new_button.isEnabled():
                    new_button.setFocus()
                    return
                new_col -= 1
        elif event.key() == QtCore.Qt.Key_Right:
            new_col = col + 1
            while new_col < max_col:
                new_button = self.ui_window.buttons[(row, new_col)]
                if new_button.isEnabled():
                    new_button.setFocus()
                    return
                new_col += 1
        elif event.key() == Qt.Key_Space:
            self.click()
            
        else:
            super().keyPressEvent(event)
